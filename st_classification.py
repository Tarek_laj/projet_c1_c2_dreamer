import numpy as np
from itertools import product
import os
from classification_utils import (
    run_classification,
    get_data
)
from params import (
    data_path,
    save_path,
    classification_mode,
    stat,
    clf_name,
    n_perms,
    test,
    outer_cv
)
print('Params info : \n classification mode = {cl} \n Test mode : {test}'.format(cl=classification_mode,
                                                                                 test=str(test)))
print('Classifier: {clf} \n stat: {sts} with n_perms={n} '.format(clf=clf_name,
                                                                     sts=str(stat),
                                                                     n=str(n_perms)))

p='col2'
moy=0

if test==False:
    stade=['awa','s1','s2','sws','rem'];
    Cds=['c1','c2','c3']

else:
    stade=['awa'];#,'s2','sws','rem'
    Cds=['c2']
sDreamer = np.arange(18) +1
sNnDreamer = np.arange(18)+19
gps=np.arange(36)
for C,st in product(Cds,stade):
    print('loading {c} for p: {cl} in {st} stage'.format(c=C,cl=p,st=st))
    X,yy,grp,sz=get_data(list1=sDreamer,
                        list2=sNnDreamer,
                        group=list(np.arange(36)+1),
                        data_path=data_path,
                        C=C,
                        stade=st.lower(),
                        moy=moy,
                        p=p)
    print (len(X))
    print(X[0].shape)

    x_data,y,groups=np.array([]),np.array([]),np.array([])
    for i,x in enumerate(X):
        x_data=np.concatenate((x_data,x),axis=0) if x_data.size else x
        y=np.concatenate((y,yy[i]),axis=0) if y.size else yy[i]
        groups=np.concatenate((groups,grp[i]),axis=0) if groups.size else grp[i]

    test_scores,permutation_scores,pvalues=run_classification(clf_name=clf_name,
                                                              X=x_data,
                                                              y=y,
                                                              outer_cv=outer_cv,
                                                              mode=classification_mode,
                                                              groups=groups,
                                                              stat=stat,
                                                              nb_permutations=n_perms,
                                                              n_jobs=-1)


    sv=save_path+'/{col}/single_trial_SF/DA'.format(col=p)
    if not os.path.exists(sv):
        os.makedirs(sv)
    np.save(sv+'/DA_{C}_{clf}_{sd}_{cl}.npy'.format(C=C,clf=clf_name,sd=st,cl=p),
    test_scores)
    np.save(sv+'/DA_perm_{C}_{clf}_{sd}_{cl}.npy'.format(C=C,clf=clf_name,sd=st,cl=p),
    permutation_scores)
    np.save(sv+'/pvals_{C}_{clf}_{sd}_{cl}.npy'.format(C=C,clf=clf_name,sd=st,cl=p),
    pvalues)
