# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 12:14:09 2017

@author: karim
"""
import os
import numpy as np
from pylab import *
from itertools import product
import scipy.io as sio
import matplotlib.pyplot as plt
#%matplotlib notebook
#from mpl_toolkits.axes_grid1 import make_axes_locatable
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from mne import *
from ttest_perm_indep import ttest_perm_unpaired
from classification_utils import get_classifier,run_classification,load_data
from sklearn.model_selection import StratifiedKFold




path='/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/sleep/cumulants'

save_path='/home/karim/Projet_C1_C2_dreamer/Classification_Results/'
if not os.path.exists(save_path):
    os.mkdir(save_path)
#path='/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/'
sensors_pos = sio.loadmat('/home/karim/MATLAB/Work/Projet C1 C2/Coord_2D_Slp_EEG.mat')['Cor']
ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']
moy=1
classification_mode='sf'
stat=True
clf_name='RBF_svm'
test=False
n_perms=1000
outer_cv= StratifiedKFold(5)
print('Params info : \n classification mode = {cl} \n Test mode : {test}'.format(cl=classification_mode,
                                                                                 test=str(test)))
print('Classifier: {clf} \n stat: {sts} with n_perms={n} '.format(clf=clf_name,
                                                                     sts=str(stat),
                                                                     n=str(n_perms)))

if test==False:
    stade=['awa','s1','s2','sws','rem'];
    Cds=['c1','c2','c3']
    cols=['col1','col2','col3','col4']
else:
    stade=['s2'];
    Cds=['c2']
    cols=['col1']

for col in cols:

    for C in Cds:
        for st in stade:
            print('Running classification {c} {cl} {st} '.format(c=C,cl=col,st=st))
            sDreamer = np.arange(18) +1
            sNnDreamer = np.array([k for k in range(19, 37)])
            Dr_data=np.array([])
            nDr_data=np.array([])

            for s1 in range(sDreamer.shape[0]):

                M1=load_data(sDreamer[s1],C,st,moy,path,col)
                Dr_data=np.concatenate((Dr_data,M1),axis=0) if Dr_data.size else M1

            for s2 in range(sNnDreamer.shape[0]):
                M2=load_data(sNnDreamer[s2],C,st,moy,path,col)
                nDr_data=np.concatenate((nDr_data,M2),axis=0) if nDr_data.size else M2

            X=np.vstack((Dr_data,nDr_data))
            y=np.concatenate([np.ones(Dr_data.shape[0]),np.zeros(nDr_data.shape[0])],axis=0)

            test_scores,permutation_scores,pvalues=run_classification(clf_name=clf_name,
                                                                    X=X,
                                                                    y=y,
                                                                    outer_cv=outer_cv,
                                                                    mode=classification_mode,
                                                                    stat=stat,
                                                                    nb_permutations=n_perms,
                                                                    n_jobs=-1)

            sv=save_path+'/{col}'.format(col=col)
            if not os.path.exists(sv):
                os.mkdir(sv)

            np.save(sv+'/DA/DA_SF_{clf}_{C}_{sd}_{cl}.npy'.format(clf=clf_name,C=C,sd=st,cl=col),
            test_scores)
            np.save(sv+'/DA/DA_perm_{clf}_{C}_{sd}_{cl}.npy'.format(clf=clf_name,C=C,sd=st,cl=col),
            permutation_scores)
            np.save(sv+'/DA/pvals_{clf}_{C}_{sd}_{cl}.npy'.format(clf=clf_name,C=C,sd=st,cl=col),
            pvalues)
