    # -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 12:14:09 2017

@author: karim
"""
import os
import numpy as np
#from pylab import *
from itertools import product
import scipy.io as sio
from classification_utils import get_classifier,run_classification,load_pe,load_C1C2
from sklearn.model_selection import StratifiedKFold,cross_val_score,permutation_test_score
#from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from params import data_path,save_path

#pe_path='/home/karim/MATLAB/Work/Projet C1 C2/permutation entropy/entropy_per_stage/'
#path='/home/karim/projet_c1_c2_dreamer/Cumulants/'

#path='/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/'
#sensors_pos = sio.loadmat('/home/karim/projet_c1_c2_dreamer/Coord_2D_Slp_EEG.mat')['Cor']
#ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
#               'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']
moy=1
classification_mode='mf'
add_pe=False
stat=True
n_perms=1000

n_jobs=-1
clf_name='RF'
test=False
outer_cv= StratifiedKFold(10)
inner_cv=StratifiedKFold(5)
print('Params info : \n classification mode = {cl} \n Test mode : {test}'.format(cl=classification_mode,
                                                                                 test=str(test)))
print('Classifier: {clf} \n stat: {sts} with n_perms={n} '.format(clf=clf_name,
                                                                     sts=str(stat),
nb_rep=1000                                                                     n=str(n_perms)))
Features_importance=True
permutation_test=False

if test==False:
    stade=['AWA','S1','S2','SWS','Rem'];
    Cds=['c1','c2']
    cols=['col1','col2','col3','col4']
else:
    stade=['S2'];
    Cds=['c1']
    cols=['col1']
for n in range(nb_rep):
    for col, st in product (cols,stade):
        save_path_f=os.path.join(save_path,'{}/MultiFeatures/'.format(col))
        if not os.path.exists(save_path_f):
            os.mkdir(save_path_f)
        for C in Cds:


            save_file=os.path.join(save_path_f,'MF_ALL_elect_{C}_{st}_{clf}_optm_rep{n}.mat'.format(n=str(n),C=C,st=st,clf=clf_name))
            print('loading data  {c} {cl} {st} '.format(c=C,cl=col,st=st))
            sDreamer = np.arange(18) +1
            sNnDreamer = np.array([k for k in range(19, 37)])
            Dr_data=np.array([])
            nDr_data=np.array([])
            for s1,s2 in zip (range(sDreamer.shape[0]),range(sNnDreamer.shape[0])):

                M1=load_C1C2(sDreamer[s1],C,st.lower(),moy,data_path,col)
                Dr_data=np.concatenate((Dr_data,M1),axis=0) if Dr_data.size else M1
                M2=load_C1C2(sNnDreamer[s2],C,st.lower(),moy,data_path,col)
                nDr_data=np.concatenate((nDr_data,M2),axis=0) if nDr_data.size else M2
            x=np.vstack((Dr_data,nDr_data))
            print(x.shape)
            y=np.concatenate([np.ones(Dr_data.shape[0]),np.zeros(nDr_data.shape[0])],axis=0)
            clf=get_classifier(clf_name='RF', inner_cv=inner_cv)

            if Features_importance:
                clf.fit(x, y)
                feat_importances =clf.best_estimator_.feature_importances_
                print(feat_importances)
            scores = cross_val_score(clf, X=x, y=y, cv=outer_cv)
            print(np.mean(scores))
            # feat_import.append(feat_importances)
            # DA.append(np.mean(scores))
            #
            save_dict={'DA':np.mean(scores),'Feat_import':feat_importances}

            if permutation_test:

                test_score, permutation_score, pvalue = permutation_test_score(
                                                        clf, x, y,
                                                        scoring="accuracy",
                                                        cv=outer_cv,
                                                        n_permutations=n_perms,
                                                        n_jobs=n_jobs)





                print("Test accuracy = %0.4f "%(test_score))
                save_dict.update({'DA_perm':permutation_score,'DA_test_perm':test_score,'pval':pvalue})
            print('saving results in :', save_file)
        sio.savemat(save_file,save_dict)


        # permutation_scores.append(permutation_score)
        # pvalues.append(pvalue)

    # if add_pe:
    #     pndr,pdr=np.array([]),np.array([])
    #     print('loading Permutation entropy: ')
    #     for s1,s2 in zip(range(sDreamer.shape[0]),range(sNnDreamer.shape[0])):
    #         p1=load_pe(path=pe_path,subject=sDreamer[s1],stade=st,moy=moy)
    #         pdr=np.concatenate((pdr,p1),axis=0) if pdr.size else p1
    #         p2=load_pe(path=pe_path,subject=sNnDreamer[s2],stade=st,moy=moy)
    #         pndr=np.concatenate((pndr,p2),axis=0) if pndr.size else p2
    #     xpe=np.vstack((pdr,pndr))
    #     x_data=np.dstack((x_data,xpe))
