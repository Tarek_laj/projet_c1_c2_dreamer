
# coding: utf-8

# # Import librairies

# In[1]:

from numpy import *
from pylab import *
from itertools import product
import os
import time
import scipy.io as sio
import matplotlib.pyplot as plt
#%matplotlib notebook
from matplotlib import gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from mne import *


# # Setup Functions

# In[2]:

def scipy_ttest_ind(cond1, cond2):
    from scipy.stats import ttest_ind
    stat, p_vals = ttest_ind(cond1, cond2, permutations=1000)
    return p_vals, stat

def scipy_ttest_rel(cond1, cond2):
    from scipy.stats import ttest_rel
    stat, p_vals = ttest_rel(cond1, cond2)
    return p_vals, stat

def fdr(test_func):
    def corr_p_vals(cond1, cond2):
        from mne.stats import fdr_correction
        p_vals, stat = test_func(cond1, cond2)
        _, p_vals_corr = fdr_correction(p_vals)
        return p_vals_corr, stat
    return corr_p_vals

def GetStatMask(cond1, cond2, stat_test_func, p_thresh=0.05):
    """Produce sensor-level binary statistical mask"""
    # ----------------------------------------------------------------- #
    p_vals, stat = stat_test_func(cond1, cond2)
    mask = np.array(p_vals <= p_thresh)
    return mask, p_vals, stat

# -- Define function that can read info from both fif and ds files -- #
import contextlib

# -- Works both in python2 and python3 -- #
try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO
# --------------------------------------- #
    
@contextlib.contextmanager
def nostdout():
    save_stdout = sys.stdout
    sys.stdout = StringIO()
    yield
    sys.stdout = save_stdout

def read_info_custom(fname):
    """Read info from .fif or from .ds"""
    from os.path import splitext
    _,ext = splitext(fname)
    if ext == '.fif':
        from mne.io import read_info
        info = read_info(fname)
    elif ext == '.ds':
        from mne.io import read_raw_ctf
        with nostdout():
            raw = read_raw_ctf(fname)
        info = raw.info
    else:
        raise RuntimeError('Unknown format for {}'.format(fname))
    return info

ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4','Fp1','FP2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']



# In[5]:

def load_data(subject,C,stade,moy,path):
    file = '{C}_s{s}.mat'
    ck = sio.loadmat(path+file.format(C=C, s=subject))
    
    mat=np.mean(ck[C+ '_' + stade],axis=0)
    #M1=np.delete(mat,[7,8],0)
    M=np.reshape(mat,(1,19))
    return np.array(M)
    


# In[ ]:




# # Statistiques 

# In[7]:

################################### Load data ################################

#path = '/home/karim/MATLAB/Work/Projet C1 C2/gamint par stade/C1_C2_hmin_gamint_par_stade/'
#path='/home/karim/MATLAB/Work/Projet C1 C2/gamint par stade/C1_C2_hmins_gamint07/'

#path='/home/karim/MATLAB/Work/Projet C1 C2/gamint par stade/gamint_par_stade_03032017/C1C2/'
#savepath ='/home/karim/MATLAB/Work/Projet C1 C2/gamint par stade/gamint_par_stade_03032017/stat/'
#path='/home/karim/MATLAB/Work/Projet C1 C2/gamint par stade/gamint0_06032017/C1C2/'
#savepath='/home/karim/MATLAB/Work/Projet C1 C2/gamint par stade/gamint0_06032017/stats/'
path='/home/karim/MATLAB/Work/Projet C1 C2/Hurst Coeff/H_par_stade/'
savepath='/home/karim/MATLAB/Work/Projet C1 C2/Hurst Coeff/'

#path='/home/karim/MATLAB/Work/Projet C1 C2/gamint par stade/c1c2_ j8_12_30sec_gamint_0.55/C1_C2_stade/'
#savepath ='/home/karim/MATLAB/Work/Projet C1 C2/gamint par stade/c1c2_ j8_12_30sec_gamint_0.55/Statistiques/'
sensors_pos = sio.loadmat('/home/karim/MATLAB/Work/Projet C1 C2/Coord_2D_Slp_EEG.mat')['Cor']

moy=1
C="H"
st=['AWA','S1','S2','SWS','Rem']
for stade in st:
   

    sDreamer = np.arange(18) +1 
    sNnDreamer = np.array([19, 20] + [k for k in range(23, 39)])

    Dr_data=np.array([])
    nDr_data=np.array([])

    for s1 in range(sDreamer.shape[0]):

        M1=load_data(sDreamer[s1],C,stade,moy,path)
        Dr_data=np.concatenate((Dr_data,M1),axis=0) if Dr_data.size else M1

    for s2 in range(sNnDreamer.shape[0]):
        M2=load_data(sNnDreamer[s2],C,stade,moy,path)
        nDr_data=np.concatenate((nDr_data,M2),axis=0) if nDr_data.size else M2     
    ################### ttest permutations ############################


    #savepath = '/home/karim/MATLAB/Work/Projet C1 C2/gamint par stade/stat/'

    Data= Dr_data - nDr_data

    ttest, pvals,_ = stats.permutation_t_test(Data, n_permutations=1000, tail=0, n_jobs=1)
    print(pvals)
    sio.savemat(savepath+'Stats_'+C+ '_' +  stade +'_pvals.mat',{'pvals': pvals})
    sio.savemat(savepath+'Stats_'+C+ '_' + stade + '_ttest.mat',{'ttest': ttest})

    ####################################### Bar plot #########################################

    ind = np.arange(0.5,19.5,1)    # the x locations for the groups

    fig = plt.figure(figsize = (10,5))
    plt.bar(ind,ttest, color=(0.3,0.8,0.8), width=0.6,tick_label = ch_names_info,align='center')
    xlim(0,19)
    ylim(-5,5)
    plt.savefig(savepath + 'ttest_'+C+ '_' + stade + '_bar.png', dpi = 300)


    ###################################### Topo plot #####################################
    #sensors_pos = sio.loadmat('/home/karim/MATLAB/Work/Projet C1 C2/Corrd_2D_inv.mat')['Cor']
    data = ttest

    mask_default = np.full((len(data)), False, dtype=bool)
    mask = np.array(mask_default)
    mask[pvals < 0.01] = True

    mask_params = dict(marker='*', markerfacecolor='w', markersize=10) # significant sensors appearence

    fig = plt.figure(figsize = (10,5))
    #plt.subplot(1,2,1)

    ax,_ = plot_topomap(data, sensors_pos,
                        cmap='viridis_r', show=False, 
                        vmin=-5, vmax=5, mask = mask,
                        mask_params = mask_params,
                        contours=False)

    fig.colorbar(ax, shrink=0.25)
    plt.savefig(savepath +C+ '_' + stade + '_stat_001.tiff', dpi = 300)


# In[ ]:

print(Dr_data.shape)

