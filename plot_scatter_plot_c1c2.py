#scatter plot c1 c2 best electrodes :
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 12:14:09 2017

@author: karim
"""
import os
import numpy as np
from pylab import *
from itertools import product
import scipy.io as sio
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn as sns; sns.set()
import pandas as pd

ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']


def load_data(subject,C,stade,moy,path):
    file='/lin_C1C2C3_guy{s}_{st}_{cl}.mat'
    ck = sio.loadmat(path+file.format(s=subject,st=stade,cl=col))[C][0:19,:]
    mat=np.mean(ck,axis=1)
    M=np.reshape(mat,(1,19))
    return np.array(M)
path='/home/karim/projet_c1_c2_dreamer/Cumulants/'
cols=['col2','col4']
stades=['awa','s1','s2','sws','rem'];
moy=1
electrode=False

for col in cols:
    for st in stades:
        sDreamer = np.arange(18) +1
        sNnDreamer = np.array([k for k in range(19, 37)])
        C1_Dr_data,C2_Dr_data=np.array([]),np.array([])
        C1_nDr_data,C2_nDr_data=np.array([]),np.array([])

        for s1 in range(sDreamer.shape[0]):

            Md1=load_data(sDreamer[s1],'c1',st,moy,path)
            C1_Dr_data=np.concatenate((C1_Dr_data,Md1),axis=0) if C1_Dr_data.size else Md1

            Md2=load_data(sDreamer[s1],'c2',st,moy,path)
            C2_Dr_data=np.concatenate((C2_Dr_data,Md2),axis=0) if C2_Dr_data.size else Md2



        for s2 in range(sNnDreamer.shape[0]):
            Mn1=load_data(sNnDreamer[s2],'c1',st,moy,path)
            C1_nDr_data=np.concatenate((C1_nDr_data,Mn1),axis=0) if C1_nDr_data.size else Mn1

            Mn2=load_data(sNnDreamer[s2],'c2',st,moy,path)
            C2_nDr_data=np.concatenate((C2_nDr_data,Mn2),axis=0) if C2_nDr_data.size else Mn2



        print(st,col)

        C1=np.concatenate((C1_Dr_data,C1_nDr_data),axis=0)
        C2=np.concatenate((C2_Dr_data,C2_nDr_data),axis=0)
        labels=['HRDF']*C1_Dr_data.shape[0] + ['LDRF']*C1_nDr_data.shape[0]
        print(C1.shape)
        print(C2.shape)
        if electrode:
            for id_elect,elect in enumerate(ch_names_info):
                print(elect)
                df_dict={'C1':list(C1[:,id_elect]),'C2':list(C2[:,id_elect]),'label':labels}
                data=pd.DataFrame(df_dict)
                plt.figure()
                sns.pairplot(x_vars=['C1'],
                             y_vars=['C2'],
                             data=data,
                             hue="label",
                             markers=['o','s'],
                             height=5)

                #ax = sns.scatterplot(x="C1", y="C2", hue="label",data=data)
                savefile='/home/karim/results_C1C2/figures/scatter_plots/fig3_scatter_plotC1C2_{elec}_{st}.png'.format(elec=elect,st=st)
                plt.savefig(savefile,dpi = 300)
                plt.close()
        else:

            df=pd.DataFrame(data=C1,columns=ch_names_info)
            df['label']=labels
            plt.figure()
            sns.pairplot(data=df,hue='label')

            #ax = sns.scatterplot(x="C1", y="C2", hue="label",data=data)
            savefile='/home/karim/results_C1C2/figures/scatter_plots/fig3_scatter_plotC1_All_elect_{st}.png'.format(st=st)
            plt.savefig(savefile,dpi = 300)
            plt.close()
