import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
from brainpipe.classification import *
from brainpipe.visual import *
import mne
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from scipy import stats
from random import sample
import os


def Topoplot_DA(DA=None,sensors_pos=None,masked=False,chance_level=0.05,Da_perm=None,Da_bino=None,figures_save_file=None,show_fig=False):
    if masked==True:
        mask_default = np.full((len(DA)), False, dtype=bool)
        mask = np.array(mask_default)
        if Da_perm is not None:
            nperm=Da_perm.shape[0]
            indx_chance=int(nperm-chance_level*nperm)
            daperm=np.sort(Da_perm,axis=0)
            mask[DA>=np.amax(daperm[indx_chance-2,:])] = True
        if Da_bino is not None:
            mask[DA>=Da_bino] = True



        mask_params = dict(marker='*', markerfacecolor='w', markersize=15)
        fig1 = plt.figure(figsize = (10,15))
        ax1,_ = plot_topomap(DA, sensors_pos,
                             cmap='inferno', show=show_fig,
                             vmin=0, vmax=100,
                             mask = mask,
                             mask_params = mask_params,outlines='skirt')
                             #names=elect,show_names=True)
    elif masked==False:


        fig1 = plt.figure(figsize = (10,15))
        ax1,_ = plot_topomap(DA, sensors_pos,
                             cmap='inferno', show=show_fig,
                             vmin=0, vmax=100,outlines='skirt')
    #add colorbar
    #fig1.colorbar(ax1, shrink=0.25)
    #save figure to tif file
    if figures_save_file is not None:
        plt.savefig(figures_save_file, dpi = 300)
