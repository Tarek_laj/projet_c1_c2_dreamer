# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 15:57:19 2018

@author: karim
"""


import numpy as np
import scipy.io as sio
from brainpipe.classification import *
from brainpipe.visual import *

from mne.viz import plot_topomap
from mne import pick_types, find_layout
from scipy import stats as scistat
import pandas as pd
from numpy import *
#from pylab import *
#from itertools import product
#import os
#import time
#import matplotlib.pyplot as plt
#get_ipython().magic('matplotlib notebook')
#from matplotlib import gridspec
#from mpl_toolkits.axes_grid1 import make_axes_locatable
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from mne import *




### Trying to do it with mean values, so one score per band/stage
### Shape for brainpipe must be: features accross COLUMNS
### Reshaping Px's 9/13/2018

### Fix epval in load_data f

# # Define functions


def load_data(data_path,subject_name,stage,bande,norm,mode):
    
    #Loads data split into bands, returned with zscores or amax
    
    data_path += '/' + subject_name + '/'
    
    data_file = 'PSD_{s}_all_bands.npy'
    data = np.load(data_path+data_file.format(s=stage))
    
    if bande=='Delta': 
        Px=data[0]
    elif bande=='Theta':
        Px=data[1]
    elif bande=='Alpha':
        Px=data[2]
    elif bande=='Sigma':
        Px=data[3]
    elif bande=='Beta':
        Px=data[4]  
    elif bande=='Low_gamma':
        Px=data[5]
    
    if mode == 1:
        #do z scores first and then average ---
        AvgPx = np.mean(Px,axis=0) #Calculate mean across rows
        return np.array(scistat.zscore(AvgPx,axis=0)) #And normalise
    
    if norm=='zscore1':         
        return np.array(scistat.zscore(Px,axis=1)) #norm across electrodes
    elif norm=='zscore2':
        return np.array(scistat.szcore(Px,axis=0)) #norm across epochs
    elif norm=='max':
        return np.array(np.amax(Px,axis=1))
    else:
        return np.array(Px)


#Define project name here
proj = 'CAF_400'

norm='zscore1' #'zscore1' to normalize using zscore across electrodes
            # 'zcore2' to normalise using zscore across epochs
           # 'max to normalize using maximum value across electrodes

mode=1  # mode=0 single trial 
        # mode=1 mean values 

test=1 # 1 runs test on one band/one stage... 0 for all bands/stages

data_path = '/media/karim/Sleep_Data_ML/Cafeine/{p}/PSD/'.format(p=proj)
fig_savepath = '/media/karim/Sleep_Data_ML/Cafeine/{p}/Topoplots/Stats/'.format(p=proj)


#Load subject list and labels
df = pd.read_csv('/media/karim/Sleep_Data_ML/Cafeine/{p}_Inventaire.csv'.format(p=proj))

#Complete list of subjects and labels
subs = df['Subject_id'].tolist()
lbls = df['CAF'].tolist()

#Separated DataFrames according to label status: CAF or PLACEBO
df_caf = df.loc[(df['CAF']==1)]
df_plac = df.loc[(df['CAF']==0)]

subs_caf = df_caf['Subject_id'].tolist()
subs_plac = df_plac['Subject_id'].tolist()

labels_caf = df_caf['CAF'].tolist()
labels_plac = df_plac['CAF'].tolist()


if proj == 'CAF_200':
    nb_subj = 40
    pout = 14 #1/3 des sujets
    
elif proj == 'CAF_400':
    nb_subj = 20
    pout = 7
    
elif proj == 'CAF_priv_200':
    nb_subj = 22
    pout = 7
    
    df_cafc = df.loc[(df['CAF']=='ctrl-Y')]
    df_placc = df.loc[(df['CAF']=='ctrl-N')]
    
    subs_cafc = df_cafc['Subject_id'].tolist()
    subs_placc = df_placc['Subject_id'].tolist()
    
    labels_cafc = df_cafc['CAF'].tolist()
    labels_placc = df_placc['CAF'].tolist()


#Electrode number
electnum = 20

#Change test status above
if test==0:
    bands = ['Delta','Theta','Alpha','Sigma','Beta','Low_gamma']
    stages=['N1','N2','Rem'] #excluding N3 because of 0 values, & AWA
elif test==1:
    bands = ['Delta']
    stages=['Rem']

#load file with channel coordinates
load_pos = sio.loadmat('/media/karim/Sleep_Data_ML/Cafeine/Coo_caf.mat')['Cor'].T
sensors_pos = np.array([load_pos[1],load_pos[0]]).T

#Electrode names
elect=['Fp1','Fp2','F3','F4','F7','F8','C3','C4','P3','P4','O1','O2','T3','T4','T5','T6','Fz','Cz','Pz','Oz']



 
 
classifier='lda' # 
                #lda’: Linear Discriminant Analysis (LDA)
                #‘svm’: Support Vector Machine (SVM)
                #‘linearsvm’ : Linear SVM
                #‘nusvm’: Nu SVM
                #‘nb’: Naive Bayesian
                #‘knn’: k-Nearest Neighbor
                #‘rf’: Random Forest
                #‘lr’: Logistic Regression
                #‘qda’: Quadratic Discriminant Analysis
     
    

DA_save_path="/media/karim/Sleep_Data_ML/Cafeine/{p}/DA/".format(p=proj)
# path where to save Decoding Accuracies 


 # Run Leave p subject Out classification and plot results
    
########################################### load features ####################################################
for st in stages:
    for b in bands:
        X,y=[],[]
        for ix,sub in enumerate(subs_caf):
            #2 vars: 1 Px pour Caf et 1 Px pour Plac
            
            Pxc=load_data(data_path,sub,st,b,norm,mode).reshape(1,electnum)
            Pxp=load_data(data_path,subs_plac[ix],st,b,norm,mode).reshape(1,electnum)
            if True not in np.isnan(Pxc) and True not in np.isnan(Pxp):
                if Pxc.shape[0] != 0 and Pxp.shape[0] != 0:
                    
                    Px = np.concatenate([Pxc,Pxp],axis=0)
                    X.append(Px) 
                    print(Px.shape)
                    
                    if mode == 0:
                        lbl_lst = np.concatenate([[labels_caf[ix]]*Pxc.shape[0],
                                              [labels_plac[ix]]*Pxp.shape[0]])
                    elif mode == 1:
#                        lbl_c = np.array([labels_caf[ix]]*20).reshape(epval,electnum)
#                        lbl_p = np.array([labels_plac[ix]]*20).reshape(epval,electnum)
#                        lbl_lst = np.concatenate((lbl_c,lbl_p),axis=0)
                        lbl_lst = np.concatenate(([[labels_caf[ix]],[labels_plac[ix]]]),axis=0)
                   
                    y.append([1]*Pxc.shape[0]+[0]*Pxp.shape[0]) #Also tried y.append([0,1])
                    
        
########################################## Run Classification ##################################################
#        print(len(x),len(y))        
        print('Running lpso for {b} in {st}'.format(b=b,st=st))
        # Classification object :
#        print(x[0].shape)
        lpso = LeavePSubjectOut(y,nb_subj,pout=pout,clf=classifier) # 
        # Run classification :
        da, pvalue, daperm = lpso.fit(X, grp=elect , method='label_rnd', n_jobs=-1)  #method=randomly shuffle the labels
        #add arg: n_perm (see brainpipe documentation) = 1000 not mandatory but needs to be done... if it doesn't work it's okay
        sio.savemat(DA_save_path +'DA_{st}_{b}_{cl}.mat'.format(st=st,b=b,cl=classifier), {'DA':da} ) 
        print(da)


##########################################     Plot Results  #################################################
#    # bar plot with statistics chance levels 
#    fig = plt.figure(figsize=(15,10))
#    lpso.daplot(da, daperm=daperm, chance_method='perm', rmax=['top', 'right'],
#                 dpax=['bottom', 'left'],
#                 cmap='magma',
#                 ylim=[0,100], 
#                 chance_unique=True,
#                 chance_level = 0.01,
#                 chance_color='darkgreen')
#    
#    plt.savefig(figures_save_file + 'barplot_'+bd+ '_' + Cond1 + '_' + Cond2 + '_'  + classifier+'_01.tif', dpi = 300)
#    
#    # Topoplots of Decoding accuracies
#
#
#    # creating mask for significant channels
#    mask_default = np.full((len(da)), False, dtype=bool)
#    mask = np.array(mask_default)
#    mask[da >=np.amax(daperm)] = True
#    mask_params = dict(marker='*', markerfacecolor='w', markersize=15)
#    #ploting figure
#    fig1 = plt.figure(figsize = (10,15))
#    ax1,_ = plot_topomap(da, sensors_pos,
#                        cmap='inferno', show=False, vmin=50, vmax=100, mask = mask, mask_params = mask_params)
#                       # names=elect,show_names=True)
#    #add colorbar
#    fig1.colorbar(ax1, shrink=0.25)
#    #save figure to tif file
#    plt.savefig(fig_savepath + 'topo_{st}_{b}_classifier.tif' + 'topo_'+bd+ '_' + Cond1 + '_' + Cond2 + '_'  + classifier+'_01.tif', dpi = 300)
#    #del da, daperm, C1, C2
#    plt.close("all")
