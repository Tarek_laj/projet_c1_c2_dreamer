#scatter plot c1 c2 best electrodes :
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 12:14:09 2017

@author: karim
"""
import os
import numpy as np
#from pylab import *
#from itertools import product
import scipy.io as sio
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn as sns; sns.set()
import pandas as pd

ch_names =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
           'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']

def hyp_epoching(hyp=[],epoch_length=30):
    m=1
    n=1
    hyp_norm=np.array([])
    while m<len(hyp-epoch_length):
        hyp_norm=np.hstack((hyp_norm,hyp[m])) if hyp_norm.size else hyp[m]
        n+=1
        m+=30
    return hyp_norm


path='/home/karim/projet_c1_c2_dreamer/c1c2_full_night/'
C_to_load='C1'
# subjects=['s1','s2','s3','s4','s5','s6','s7','s8','s9','s10','s11','s12',
#         's13','s14','s15','s16','s17','s18','s19','s20','s21','s22','s23',
#         's24','s25','s26','s28','s29','s30','s31','s32','s33',
#         's34','s35','s36','s37']

subjects=['s1']
if C_to_load=='C2':
    vmin=-0.1
    vmax=0
else:
    vmin=0
    vmax=.8
p=2
for s in subjects:
    data_name=os.path.join(path,'{s}_cumulants_911.mat'.format(s=s))
    Ck=sio.loadmat(data_name)
    hyp=np.loadtxt('/home/karim/projet_c1_c2_dreamer/hypnogrammes/hyp_per_{s}.txt'.format(s=s))
    hyp_norm=hyp_epoching(hyp=hyp,epoch_length=30)
    C=np.squeeze(Ck[C_to_load][p-1,:,:])
    ix1=np.where(hyp_norm==-1)[0]
    ix2=np.where(hyp_norm==-2)[0]
    hyp_final=np.delete(hyp_norm,np.hstack((ix1,ix2)))
    Cx=np.delete(C,np.hstack((ix1,ix2)),axis=1)
    hyp_final[np.where(hyp_final==4)[0]]=3
    hyp_final[np.where(hyp_final==5)[0]]=4
    fig= plt.figure(figsize=(20,15))
    gs = gridspec.GridSpec(6, 6)
    ax1 = plt.subplot(gs[:-2, :])
    p0 = ax1.get_position().get_points().flatten()
    print(p0)
    cb_ax = fig.add_axes([0.93, 0.4, 0.02, 0.45])
    sns.heatmap(Cx,
                cmap='summer',
                vmin=vmin,vmax=vmax,
                cbar=True,
                cbar_ax=cb_ax,
                ax=ax1)
    print(Cx.shape)
    ax1.set_yticklabels(ch_names,rotation='horizontal',fontsize=16)
    ax1.set_xticklabels(['']*Cx.shape[1])
    #################### hypnogramme ###################################
    ax2=plt.subplot(gs[-2:,:])
    #ax2.patch.set_facecolor('w')
    plt.step(np.linspace(0,len(hyp_final)-1,len(hyp_final)),hyp_final)
    plt.yticks(np.arange(0,5),('AWA','S1','S2','SWS','Rem'),fontsize=16)
    plt.xlabel('epochs (30sec)',fontsize=14)
    plt.xlim(0,len(hyp_final)-1)
    plt.grid(False)
    #plt.show()
    plt.savefig('/home/karim/results_C1C2/figures_finales/c1c2_full_night/plot_{c}_p{p}_hypnogramme_{s}_911.png'.format(c=C_to_load,p=str(p),s=s),dpi=300)
    plt.close()










#
