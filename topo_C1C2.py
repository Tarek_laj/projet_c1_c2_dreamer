# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 12:14:09 2017

@author: karim
"""
import os
import numpy as np
from pylab import *
from itertools import product
import scipy.io as sio
import matplotlib.pyplot as plt
#%matplotlib notebook
from matplotlib import gridspec
#from mpl_toolkits.axes_grid1 import make_axes_locatable
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from mne import *
from ttest_perm_indep import ttest_perm_unpaired

ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']


def load_data(subject,C,stade,moy,path):
    file='/lin_C1C2C3_guy{s}_{st}_{cl}.mat'
    #file = '/{c}/C1C2C3_S{s}_{st}_{c}.mat'
    ck = sio.loadmat(path+file.format(s=subject,st=stade,cl=col))[C][0:19,:]
    mat=np.mean(ck,axis=1)
    M=np.reshape(mat,(1,19))
    return np.array(M)
path='/home/karim/projet_c1_c2_dreamer/Cumulants/'
#path='/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/sleep/cumulants'
#path='/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/'
cols=['col1','col2','col3','col4']
#cols=['col2']
sensors_pos = sio.loadmat('/home/karim/projet_c1_c2_dreamer/Coord_2D_Slp_EEG.mat')['Cor']
Dr=['Dr','Ndr']
stade=['awa','s1','s2','sws','rem'];
moy=1
Cds=['c1','c2']
stat=False

for col in cols:
    savepath = '/home/karim/results_C1C2/topoplots/{}/'.format(col)
    if not os.path.exists(savepath):
        os.makedirs(savepath)
    for C in Cds:


        if C=='c1':
            if col=='col1':
                minax=0
                maxax=3.5
            elif col=='col2':
                minax=0
                maxax=1.5
            elif col=='col3':
                minax=0
                maxax=1
            elif col=='col4':
                minax=0
                maxax=1
        elif C=='c2':
            minax=-0.1
            maxax=0

        if stat:
            minax=-2
            maxax=2

        for st in stade:
            sDreamer = np.arange(18) +1
            sNnDreamer = np.array([k for k in range(19, 37)])
            Dr_data=np.array([])
            nDr_data=np.array([])

            for s1 in range(sDreamer.shape[0]):

                M1=load_data(sDreamer[s1],C,st,moy,path)
                Dr_data=np.concatenate((Dr_data,M1),axis=0) if Dr_data.size else M1

            for s2 in range(sNnDreamer.shape[0]):
                M2=load_data(sNnDreamer[s2],C,st,moy,path)
                nDr_data=np.concatenate((nDr_data,M2),axis=0) if nDr_data.size else M2
            print(Dr_data.shape)
            print(nDr_data.shape)

            if stat:
                data, pvals = ttest_perm_unpaired(Dr_data, nDr_data, n_perm=1000, correction="maxstat")

                mask_default = np.full((len(data)), False, dtype=bool)
                mask = np.array(mask_default)
                mask[pvals <= 0.05] = True
                mask_params = dict(marker='*', markerfacecolor='w', markersize=10) # significant sensors appearence

                fig = plt.figure(figsize = (15,15)) #vmin=minax,vmax=maxax,
                ax,_ = plot_topomap(data,sensors_pos,
                                    vmin=minax,vmax=maxax,
                                    cmap='jet',
                                    show=False,
                                    contours=False,
                                    mask = mask,
                                    mask_params = mask_params,
                                    names=ch_names_info,show_names=False)

                #fig.colorbar(ax)
                #plt.show()
                plt.savefig(savepath + 'topo_stat_{c}_{C}_{st}.tiff'.format(c=col,C=C,st=st), dpi = 300)

            else:
                data=np.dstack((Dr_data,nDr_data))

                for i in range(data.shape[2]):

                    fig = plt.figure(figsize = (15,15))
                    ax,_ = plot_topomap(np.mean(data[:,:,i],axis=0), sensors_pos,
                                        cmap='seismic', show=False,
                                        vmin=minax,vmax=maxax,
                                        mask = None,
                                        contours=False,
                                        names=ch_names_info,show_names=True)

                    #fig.colorbar(ax)
                    plt.savefig(savepath + 'topo_' + C + '_' + st + '_' + Dr[i] + '_{c}.tiff'.format(c=col), dpi = 300)
                    plt.close()
            print(stat,st,C,col)
