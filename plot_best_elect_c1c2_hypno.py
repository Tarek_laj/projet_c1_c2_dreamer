#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  7 23:59:18 2019

@author: karim
"""

import os
import numpy as np
#from pylab import *
#from itertools import product
import scipy.io as sio
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn as sns; sns.set()
import pandas as pd

ch_names =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
           'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']

def hyp_epoching(hyp=[],epoch_length=30):
    m=1
    n=1
    hyp_norm=np.array([])
    while m<len(hyp-epoch_length):
        hyp_norm=np.hstack((hyp_norm,hyp[m])) if hyp_norm.size else hyp[m]
        n+=1
        m+=30
    return hyp_norm

def get_elect_index(ch_names=[],elect_to_select=[]):
    for ind,ch in enumerate(ch_names):
        if ch==elect_to_select:
            return ind
path='/home/karim/projet_c1_c2_dreamer/'
C_to_load='C1'
elect_to_select='F3'
subjects=['s1'] #,'s30'
if C_to_load=='C2':
    vmin=-0.05
    vmax=0
else:
    vmin=0
    vmax=0.8
p=4
for s in subjects:
    data_name=os.path.join(path,'{s}_cumulants.mat'.format(s=s))
    Ck=sio.loadmat(data_name)
    hyp=np.loadtxt('hyp_per_{s}.txt'.format(s=s))
    hyp_norm=hyp_epoching(hyp=hyp,epoch_length=30)
    C=np.squeeze(Ck[C_to_load][p-1,:,:])
    
    ix1=np.where(hyp_norm==-1)[0]
    ix2=np.where(hyp_norm==-2)[0]
    hyp_final=np.delete(hyp_norm,np.hstack((ix1,ix2)))
    Cx=np.delete(C,np.hstack((ix1,ix2)),axis=1)
    hyp_final[np.where(hyp_final==3)[0]]=4

    #df=pd.DataFrame(data=Cx.T,columns=ch_names)
    fig= plt.figure(figsize=(20,15))

    gs = gridspec.GridSpec(3, 1)
    ax1 = plt.subplot(gs[:-2, :])
    time=np.linspace(0,len(hyp_final)-1,len(hyp_final)-1)
    ind_ch=get_elect_index(ch_names=ch_names,elect_to_select=elect_to_select)
    plt.plot(time,Cx[ind_ch,:],color='r')
    plt.xlim(0,len(hyp_final)-1)
    plt.grid(False)



    ax2=plt.subplot(gs[-2:,:])
    plt.step(time,hyp_final[:-1])
    plt.yticks(np.arange(0,6),('AWA','S1','S2','','SWS','Rem'))
    plt.xlim(0,len(hyp_final)-1)

#    ax2.patch.set_facecolor('white')
#    ax2.patch.set_edgecolor('black')
    plt.grid(False)
    plt.savefig('/home/karim/results_C1C2/figures_finales/plot_{c}_p{p}_{elec}_hypnogramme_{s}.png'.format(elec=elect_to_select,c=C_to_load,p=str(p),s=s),dpi=300)







#
