import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import matplotlib.gridspec as gridspec

clf_name='LDA'
C='c2'
Classes=['Dr','nDr']
stades=['AWA','S1','S2','SWS','Rem']
st_n=['AWA','N1','N2','N3','Rem']
test=True
if test:
    Cols=['col2']
else:
    Cols=['col1','col2','col3','col4']
classif='SFS'
# DA_file='/home/karim/results_C1C2/Resultats_feat_select/\
# {classif}/feat_import_topo_{clf}_{c}_{p}_{st}.png'

DA_file='/home/karim/results_C1C2/figures/SFS_topo_{clf}_{c}_{p}_{st}.png'
def crop(image_path=None, saved_location=None):

    im = Image.open(image_path)
    w, h = im.size
    print(w,h)
    #im.show()
    coords=(900,50,w-500,h-50)
    cropped_image = im.crop(coords)
    if isinstance(saved_location,str):
        cropped_image.save(saved_location)
    #cropped_image.show()
    return cropped_image

for p in Cols:
    fig=plt.figure(constrained_layout=True)
    gs = gridspec.GridSpec(1, 5,figure=fig)

    for ind,st in enumerate(stades):

        topo_DA=DA_file.format(p=p,st=st,clf=clf_name,classif=classif,c=C)
        dr_img =  crop(image_path=topo_DA)
        ax1 = fig.add_subplot(gs[0, ind])
        ax1.imshow(dr_img)
        ax1.set_title(st_n[ind])
        ax1.axis('off')
        ax1.set_ylabel(st,rotation='horizontal')
        savefile='/home/karim/results_C1C2/figures/figtopo_{classif}_p{cl}_{c}.png'.format(classif=classif,cl=p,c=C)
        #plt.show()
        plt.savefig(savefile,dpi=650)
