clear
clc
data=load('/home/karim/Projet_C1_C2_dreamer/lin_sleep_cumulants.mat');
stades={'awa','s1','s2','sws','rem'};%,'s2','sws','rem'


% data=load('lin_uncons_cumulants.mat');
% stades={'consc','unconsc'};
cl=1;
for st=1:length(stades)
   
   subject_names=fieldnames(getfield(data,stades{st}));
   sub_struct=getfield(data,stades{st});
   for sub=1:numel(subject_names)
      
     elect_struc=getfield(sub_struct,subject_names{sub});

     electrodes=fieldnames(elect_struc);
     c1=[];
     c2=[];
     c3=[];
     for el=1: numel(electrodes)
        
        epoch_struct=getfield(elect_struc,electrodes{el});
        
        epochs=struct2cell(epoch_struct);
        x1=[];
        x2=[];
        x3=[];
        for e=1:numel(epochs)
            x1(e)=epochs{e}(1,cl);
            x2(e)=epochs{e}(2,cl);
            x3(e)=epochs{e}(3,cl);
            
        end
        c1=[c1;x1];
        c2=[c2;x2];
        c3=[c3;x3];
        
     end
    save(['/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/sleep/lin_C1C2C3_' subject_names{sub} '_' stades{st} '_col' int2str(cl) '.mat'],'c1','c2','c3')
    clear c1  c2  c3
    end
end

