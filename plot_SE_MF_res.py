import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import matplotlib.gridspec as gridspec

clf_name='LDA'


stades=['AWA','S1','S2','SWS','Rem']
test=False
if test:
    Cols=['col2']
else:
    Cols=['col1','col2','col3','col4']
#classif='single_feature_single_electrode'
classif='multifeat_single_electrode'
# topo_mean_file='/home/karim/results_C1C2/topoplots/{p}/topo_{c}_{st}_{clas}_{p}.tiff'
# tval_file='/home/karim/results_C1C2/topoplots/{p}/topo_stat_{p}_{c}_{st}.tiff'
DA_file='/home/karim/results_C1C2/Classification_Results/{p}/\
{classif}/Topoplots/Topo_C1C2_{clf}_{st}.tiff'

def crop(image_path=None, saved_location=None):

    im = Image.open(image_path)
    w, h = im.size
    print(w,h)
    #im.show()
    coords=(900,50,w-500,h-50)
    cropped_image = im.crop(coords)
    if isinstance(saved_location,str):
        cropped_image.save(saved_location)
    #cropped_image.show()
    return cropped_image

for p in Cols:
    fig=plt.figure(constrained_layout=True)
    gs = gridspec.GridSpec(1, 5,figure=fig)

    for ind,st in enumerate(stades):
        #fig, axs = plt.subplots(1, 4, sharey=True)
        # add a big axes, hide frame
        # fig.add_subplot(111, frameon=False)
        # #hide tick and tick label of the big axes
        # plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
        # plt.grid(False)
        # plt.xlabel("common X")


        topo_DA=DA_file.format(p=p,clf=clf_name,c=C,st=st.lower(),classif=classif)
        DA_img=crop(image_path=topo_DA)

        ax = fig.add_subplot(gs[0,ind])
        ax.set_title(st)
        #ax.imshow(DA_img)
        ax.axis('off')
        #axs.set_ylabel()
        #fig.text(0.04, 0.5, 'common Y', va='center', rotation='horizontal')
    savefile='/home/karim/results_C1C2/figures/figure_{classif}_p{cl}.png'.format(classif=classif,cl=p)
    #plt.show()
    plt.savefig(savefile,dpi=300)
