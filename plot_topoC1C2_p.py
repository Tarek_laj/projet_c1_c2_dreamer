import os
import numpy as np
from pylab import *
from itertools import product
import scipy.io as sio
import matplotlib.pyplot as plt
from matplotlib import gridspec
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from mne import *
from ttest_perm_indep import ttest_perm_unpaired

ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']


def load_data(subject,C,stade,moy,path):
    file='/lin_C1C2C3_guy{s}_{st}_{cl}.mat'
    ck = sio.loadmat(path+file.format(s=subject,st=stade,cl=col))[C][0:19,:]
    mat=np.mean(ck,axis=1)
    M=np.reshape(mat,(1,19))
    return np.array(M)

path='/home/karim/projet_c1_c2_dreamer/Cumulants/'
cols=['col1','col2','col3','col4']
sensors_pos = sio.loadmat('/home/karim/projet_c1_c2_dreamer/Coord_2D_Slp_EEG.mat')['Cor']
Dr=['Dr','Ndr']
stade=['awa','s1','s2','sws','rem'];
p_names=['p=1','p=2','p=3','p=4']
moy=1
Cds=['c2']
Dream=False

for C in Cds:
    fig=plt.figure()
    gs = gridspec.GridSpec(5, 4,figure=fig)
    gs.update(wspace=0, hspace=0)

    for idp,col in enumerate(cols):
        savepath = '/home/karim/results_C1C2/figures_finales/'
        if not os.path.exists(savepath):
            os.makedirs(savepath)
        if C=='c1':
            if col=='col1':
                minax=0
                maxax=3.5
            elif col=='col2':
                minax=0
                maxax=1.5
            elif col=='col3':
                minax=0
                maxax=1
            elif col=='col4':
                minax=0
                maxax=1
        elif C=='c2':
            minax=-0.1
            maxax=0


        for idx,st in enumerate(stade):
            if Dream==True:
                sDreamer = np.arange(18) +1
                Dr_data=np.array([])
                Dr='dreamer'
                for s1 in range(sDreamer.shape[0]):
                    M1=load_data(sDreamer[s1],C,st,moy,path)
                    Dr_data=np.concatenate((Dr_data,M1),axis=0) if Dr_data.size else M1

            else:
                sNnDreamer = np.array([k for k in range(19, 37)])
                Dr_data=np.array([])
                Dr='Nndreamer'
                for s2 in range(sNnDreamer.shape[0]):
                    M2=load_data(sNnDreamer[s2],C,st,moy,path)
                    Dr_data=np.concatenate((Dr_data,M2),axis=0) if Dr_data.size else M2

            ax = fig.add_subplot(gs[idx, idp])
            if idp==0:
                ax.set_ylabel(st.upper(),rotation=90,fontsize =12)
            if idx==0:
                ax.set_title(p_names[idp],fontsize =12)

            ax1,_=plot_topomap(np.mean(Dr_data,
                                axis=0),
                                sensors_pos,
                                cmap='seismic',
                                show=False,
                                vmin=minax,
                                vmax=maxax,
                                mask = None,
                                extrapolate='local',
                                contours=False,
                                names=ch_names_info,
                                show_names=False)
            if idx==4:
                p0 = ax.get_position().get_points().flatten()
                ax_cbar1 = fig.add_axes([p0[0]+0.05,p0[1]-0.05, 0.09, 0.01])
                fig.colorbar(ax1, cax=ax_cbar1, orientation='horizontal',ticks=[minax,maxax/2, maxax])

    plt.savefig(savepath + 'topo_all_p_{c}_{d}_siec.png'.format(c=C,d=Dr), dpi = 300)
    #plt.show()
