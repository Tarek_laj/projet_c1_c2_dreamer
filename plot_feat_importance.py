#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 29 16:04:14 2019

@author: karim
"""
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
from mne.viz import plot_topomap
import matplotlib.gridspec as gridspec
clf_name='RF'
cds=['c1','c2']
cols=['col4']
sensors_pos = sio.loadmat('/home/karim/MATLAB/Work/Projet C1 C2/Coord_2D_Slp_EEG.mat')['Cor']
stades=['AWA','S1','S2','SWS','Rem']

for col in cols:
    fig=plt.figure(figsize=(15, 5))
    #fig, axs = plt.subplots(2,5, figsize=(15, 6))
    #axes=axs.ravel()
    gs = gridspec.GridSpec(2, 5,figure=fig)
    #gs.update(wspace=0, hspace=0)

    for idc,C in enumerate(cds):
        for idst,st in enumerate(stades):
            DA=np.array([])
            feat_import=np.array([])
            for rep in range(50):
                file_DA='/home/karim/results_C1C2/feat_importances/{cl}/MultiFeatures/MF_ALL_elect_{c}_{st}_{clf}_optm_rep{r}'.format(cl=col,
                                                                                                                                    c=C,
                                                                                                                                    st=st,
                                                                                                                                    clf=clf_name,
                                                                                                                                    r=str(rep))
                #file_DA='/home/karim/results_C1C2/Multi_feat_res/{cl}/MultiFeatures/MF_ALL_elect_{c}_{st}_{clf}'.format(cl=col,c=C,st=st,clf=clf_name)
                fimp=sio.loadmat(file_DA)['Feat_import']
                feat_import=np.vstack((feat_import,fimp)) if feat_import.size else fimp
                da=sio.loadmat(file_DA)['DA']
                DA=np.vstack((DA,da)) if DA.size else da

            mean_da=np.mean(DA)
            std_da=np.std(DA)
            print(std_da,'STD')
            print(DA)
            mean_feat_import=np.mean(feat_import,axis=0)
            #print('DA',C,st,col,mean_da)
            ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                      'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']
            ax=fig.add_subplot(gs[idc,idst])
            im,sx=plot_topomap(np.squeeze(mean_feat_import.T),
                            sensors_pos,
                            names=ch_names_info,
                            show_names=False,
                            cmap='magma',
                            axes=ax,
                            show=False,
                            vmin=0,vmax=0.1,
                            contours=True)
            if idc==0:
                ax.set_title(st, fontsize=18)
                p0 = ax.get_position().get_points().flatten()
                plt.text(p0[0]-.55,p0[1]-1.25,str("DA={:.2f}%".format(100*mean_da)), fontsize=14)
            else:
                p0 = ax.get_position().get_points().flatten()
                plt.text(p0[0]-.55,p0[1]-0.8,str("DA={:.2f}%".format(100*mean_da)), fontsize=14)

            if idst==0:
                ax.set_ylabel(C.upper(),fontsize =18,rotation=90)



            #plt.text(p0[0]-0.8,p0[1]-0.65,str("DA={:.2f}%".format(100*mean_da)), fontsize=12)

            fig.subplots_adjust(right=0.8)
            cbar_ax = fig.add_axes([0.85, 0.35, 0.01, 0.3])
            plt.colorbar(im,cax=cbar_ax)



    #plt.show()
    save_topo='/home/karim/results_C1C2/figures_finales/feat_import_topo_{clf}_{c}_{p}_{st}.png'.format(clf=clf_name,
                                                                                            c=C,
                                                                                            p=col,st=st)
    #plt.savefig(save_topo, dpi = 300)
