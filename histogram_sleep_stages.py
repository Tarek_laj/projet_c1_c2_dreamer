import numpy as np
import matplotlib.pyplot as plt

import itertools


def hyp_epoching(hyp=[],epoch_length=30):
    m=1
    n=1
    hyp_norm=np.array([])
    while m<len(hyp-epoch_length):
        hyp_norm=np.hstack((hyp_norm,hyp[m])) if hyp_norm.size else hyp[m]
        n+=1
        m+=30
    ix1=np.where(hyp_norm==-1)[0]
    ix2=np.where(hyp_norm==-2)[0]
    hyp_final=np.delete(hyp_norm,np.hstack((ix1,ix2)))
    return hyp_final



dr_subjects=['s1','s2','s3','s4','s5','s6','s7','s8','s9','s10','s11','s12',
        's13','s14','s15','s16','s17','s18']
ndr_subjects=['s19','s20','s21','s22','s23',
        's24','s25','s26','s28','s29','s30','s31','s32','s33',
        's34','s35','s36','s37']
hyp_dr_all,hyp_ndr_all=np.array([]),np.array([])
for s_dr,s_ndr in zip(dr_subjects,ndr_subjects):
    hyp=np.loadtxt('/home/karim/projet_c1_c2_dreamer/hypnogrammes/hyp_per_{s}.txt'.format(s=s_dr))
    print(s_dr)
    hyp_dr=hyp_epoching(hyp=hyp,epoch_length=30)
    print(hyp_dr_all.shape)
    hyp_dr_all=np.vstack((hyp_dr_all,hyp)) if hyp_dr_all.size else hyp
    print(s_ndr)
    hyp_nd=np.loadtxt('/home/karim/projet_c1_c2_dreamer/hypnogrammes/hyp_per_{s}.txt'.format(s=s_ndr))
    hyp_ndr=hyp_epoching(hyp=hyp_nd,epoch_length=30)
    print(hyp_ndr.shape)
    hyp_ndr_all=np.vstack((hyp_ndr_all,hyp_ndr)) if hyp_ndr_all.size else hyp_ndr


print(hyp_ndr_all.shape)
print(hyp_dr_all.shape)
