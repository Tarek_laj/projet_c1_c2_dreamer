#params
import scipy.io as sio
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import LeavePGroupsOut as lpgo
import os

#data_params
os.chdir(os.path.dirname(__file__))
main_path=os.getcwd()


data_path=os.path.join(main_path,'Cumulants/')



save_path=os.path.join(main_path,'Classification_Results/')

sensors_pos = sio.loadmat(os.path.join(main_path,'Coord_2D_Slp_EEG.mat'))['Cor']
ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4',
                'FC1', 'FC2', 'CP1', 'CP2']

#classification params
classification_mode='mf_sg_elect'
stat=True
clf_name='RBF_svm'
test=False
n_perms=100
outer_cv=lpgo(n_groups=2)
inner_cv=lpgo(n_groups=2)

