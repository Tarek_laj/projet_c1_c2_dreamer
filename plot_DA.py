from classification_utils import Topo_DA
import numpy as np
import os
import scipy.io as sio
sensors_pos = sio.loadmat('/home/karim/MATLAB/Work/Projet C1 C2/Coord_2D_Slp_EEG.mat')['Cor']
#path='/home/karim/Projet_C1_C2_dreamer/Classification_Results/Permutation_entropy/'
stages=['awa','s1','s2','sws','rem']
#stages=['AWA','S1','S2','SWS','Rem']
C=['c1','c2','c3']
cols=['col1','col2','col3','col4']
clf_name='LDA'
for col in cols:
    for c in C:
        for st in stages:
            #path='/home/karim/Projet_C1_C2_dreamer/Classification_Results/{cl}/multifeat_single_electrode/DA/'.format(cl=col)
            #path='/home/karim/Projet_C1_C2_dreamer/Classification_Results/{cl}/single_feature_single_electrode/DA/'.format(cl=col)
            path='/home/karim/projet_c1_c2_dreamer/Classification_Results/{cl}/single_trial_SF/DA/'.format(cl=col)
            #DA_file=path+'DA_SF_{clf}_{st}.npy'.format(clf=clf_name,st=st)
            DA_file=path+'DA_{c}_{clf}_{st}_{cl}.npy'.format(c=c,clf=clf_name,st=st,cl=col)
            #DA_file=path+'DA_SF_{clf}_{c}_{st}_{cl}.npy'.format(c=c,clf=clf_name,st=st,cl=col)
            DA_perm_file=path+ 'DA_perm_{c}_{clf}_{st}_{cl}.npy'.format(c=c,clf=clf_name,st=st,cl=col)
            #DA_perm_file=path+ 'DA_perm_{clf}_{st}.npy'.format(clf=clf_name,st=st)
            save_path='/home/karim/projet_c1_c2_dreamer/Classification_Results/{cl}/single_trial_SF/Topoplots/'.format(cl=col)
            #save_path='/home/karim/Projet_C1_C2_dreamer/Classification_Results/Permutation_entropy/Topoplots/'
            if not os.path.exists(save_path):
                os.mkdir(save_path)
            #save_file=save_path+'/Topo_pe_{clf}_{st}.tiff'.format(clf=clf_name,st=st)
            save_file=save_path+'/DA_{clf}_{c}_{st}_{cl}'.format(clf=clf_name,c=c,st=st,cl=col)
            DA=np.load(DA_file)*100
            print(DA.shape)
            DA_perm=np.sort(np.load(DA_perm_file),axis=1)
            print(DA_perm.shape)
            DA_thr=np.max(DA_perm[:,99])*100
            print('Chance level : ', DA_thr)
            Topo_DA(DA=DA,
                    sensors_pos=sensors_pos,
                    mask=True,
                    DA_thr=DA_thr,
                    save_file=save_file)
