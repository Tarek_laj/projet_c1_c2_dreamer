import numpy as np
import os
from sklearn.model_selection import train_test_split
from mlxtend.feature_selection import SequentialFeatureSelector as SFS
#from sklearn.svm import SVC
#from sklearn.ensemble import RandomForestClassifier as RF
#from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
#from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs
from classification_utils import get_classifier
from itertools import product
from classification_utils import get_classifier,run_classification,load_pe,load_C1C2
from random import randint
from sklearn.model_selection import StratifiedKFold
from params import data_path,save_path
import pandas as pd
"""
################################################################################
##################### Set Classification Parameters ############################
################################################################################
"""
moy=1
classification_mode='feature_selection'
n_jobs=-1
clf_name='LDA'
test=False
outer_cv= StratifiedKFold(10)
local=False
plot_SFS=False
inner_cv=5



if test==False:
    stade=['AWA','S1','S2','SWS','Rem'];
    Cds=['c1','c2']
    cols=['col4','col2']
    nb_rep=10

else:
    stade=['S2'];
    Cds=['c1']
    cols=['col1']
    nb_rep=1

clf=get_classifier(clf_name=clf_name)#, inner_cv=inner_cv)

features_list=np.asarray(['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4',
                 'FC1', 'FC2', 'CP1', 'CP2'])

print('Params info : \n classification mode = {cl} \n Test mode : {test}'.format(cl=classification_mode,
                                                                                 test=str(test)))
print('Classification info : \n classifer: {clf} \n nbre of rep : {n}'.format(clf=clf_name,
                                                                              n=str(nb_rep)))

N=[]
for col, st in product (cols,stade):
    nf=randint(1,15000)
    N.append(nf)
    if local:
        save_path_f='/home/karim/results_C1C2/Classification_Results/{}/MultiFeatures/'.format(col)
        path='/home/karim/projet_c1_c2_dreamer/Cumulants/'
    else:
        save_path_f=os.path.join(save_path,'{}/MultiFeatures/'.format(col))
    if not os.path.exists(save_path_f):
        os.mkdir(save_path_f)
    DA,Selected_features=[],[]
    for C in Cds:
        print('loading data : {col},{st},{c}'.format(col=col,st=st,c=C))
        save_file=os.path.join(save_path_f,'Features_selection_ALL_elect_{C}_{st}_{clf}.mat'.format(C=C,st=st,clf=clf_name))
        sDreamer = np.arange(18) +1
        sNnDreamer = np.array([k for k in range(19, 37)])
        Dr_data=np.array([])
        nDr_data=np.array([])
        for s1,s2 in zip (range(sDreamer.shape[0]),range(sNnDreamer.shape[0])):
            M1=load_C1C2(sDreamer[s1],C,st.lower(),moy,data_path,col)
            Dr_data=np.concatenate((Dr_data,M1),axis=0) if Dr_data.size else M1
            M2=load_C1C2(sNnDreamer[s2],C,st.lower(),moy,data_path,col)
            nDr_data=np.concatenate((nDr_data,M2),axis=0) if nDr_data.size else M2
        x=np.vstack((Dr_data,nDr_data))
        print('data shape',x.shape)
        y=np.concatenate([np.ones(Dr_data.shape[0]),np.zeros(nDr_data.shape[0])],axis=0)
        results_df=pd.DataFrame([])
        for rep in range(nb_rep):
            print('Running Classification: rep = {r}, forward feature selection using {cl}'.format(r=str(rep+1),
                                                                                                  cl=clf_name))
            X_train, X_test, y_train, y_test = train_test_split(
                       x, y, test_size=0.1)
            clf.fit(X_train,y_train)
            print('Feature selection...')
            sfs1 = SFS(clf,
                       k_features='parsimonious',
                       forward=True,
                       floating=False,
                       scoring='accuracy',
                       cv=inner_cv,
                       n_jobs=-1)
            sfs1 = sfs1.fit(X_train, y_train)
            selected_f=features_list[np.array(sfs1.k_feature_idx_)]
            print('Selected features:', selected_f)
            X_train_sfs = sfs1.transform(X_train)
            X_test_sfs = sfs1.transform(X_test)
            # Fit the estimator using the new feature subset
            # and make a prediction on the test data
            clf.fit(X_train_sfs, y_train)
            y_pred = clf.predict(X_test_sfs)
            # Compute the accuracy of the prediction
            acc = float((y_test == y_pred).sum()) / y_pred.shape[0]
            print('Test set accuracy: %.2f %%' % (acc * 100))
            DA.append(acc)
            Selected_features.append(selected_f)
            df=pd.DataFrame.from_dict(sfs1.get_metric_dict()).T
            results_df.append(df)
        np.save(save_path_f+'DA_{cl}_SFS_{c}_{col}_{st}_{nj}.npy'.format(cl=clf_name,
                                                                   col=col,
                                                                   c=C,
   								                                   st=st,
                                                                   nj=str(nf)),
                                                                   DA)
        np.save(save_path_f+'Selected_features_{cl}_{c}_{col}_{st}_{nj}.npy'.format(cl=clf_name,
                                                                    col=col,
                                                                    c=C,st=st,
                                                                    nj=str(nf)),
                                                                    Selected_features)
#        df.to_excel(save_path_f+'SFS_Results_{cl}_{c}_{col}_{nj}.xlsx'.format(cl=clf_name,
#                                                                             col=col,
#                                                                             c=C,
#                                                                             nj=str(nf)))

np.save(save_path_f+'Jobs_list.npy',N)
