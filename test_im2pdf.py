import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import matplotlib.gridspec as gridspec

clf_name='LDA'
C='c1'
Classes=['Dr','nDr']
stades=['AWA','S1','S2','SWS','Rem']
test=True
if test:
    Cols=['col2']
else:
    Cols=['col1','col2','col3','col4']

topo_mean_file='/home/karim/Projet_C1_C2_dreamer/topoplots/{p}/topo_{c}_{st}_{clas}_{p}.tiff'
tval_file='/home/karim/Projet_C1_C2_dreamer/topoplots/{p}/topo_stat_{p}_{c}_{st}.tiff'
DA_file='/home/karim/Projet_C1_C2_dreamer/Classification_Results/{p}/\
single_feature_single_electrode/Topoplots/DA_SF_{clf}_{c}_{st}_{p}.png'



def crop(image_path=None, saved_location=None):

    im = Image.open(image_path)
    w, h = im.size
    print(w,h)
    #im.show()
    coords=(850,50,w-700,h-100)
    cropped_image = im.crop(coords)
    if isinstance(saved_location,str):
        cropped_image.save(saved_location)
    #cropped_image.show()
    return cropped_image

for p in Cols:
    #gs = gridspec.GridSpec(5, 4)
    fig, axs = plt.subplots(5, 4, sharey=True,sharex=True)
    #add a big axes, hide frame
    fig.add_subplot(111, frameon=False)
    #hide tick and tick label of the big axes
    plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
    plt.grid(False)
    #plt.xlabel("common X")
    for ind,st in enumerate(stades):
        topo_dr=topo_mean_file.format(p=p,c=C,st=st.lower(),clas='Dr')
        dr_img =  crop(image_path=topo_dr)
        axs[ind,0].imshow(dr_img)
        axs[0,0].set_title('HDRF',va='center')
        axs[ind,0].set_ylabel(st,rotation=0,labelpad=30,va='center')
        #axs[ind,0].axis('off')
        axs[ind,0].set_xticks([])
        axs[ind,0].set_yticks([])
        axs[ind,0].spines['left'].set_visible(False)
        axs[ind,0].spines['right'].set_visible(False)
        axs[ind,0].spines['bottom'].set_visible(False)
        axs[ind,0].spines['top'].set_visible(False)
        axs[ind,0].patch.set_visible(False)
        #axs[ind,0].text(4, 0, st , rotation='horizontal')

        topo_ndr=topo_mean_file.format(p=p,c=C,st=st.lower(),clas='Ndr')
        ndr_img =  crop(image_path=topo_ndr)
        axs[ind,1].imshow(ndr_img)
        axs[0,1].set_title('LDRF')
        axs[ind,1].axis('off')

        topo_stat=tval_file.format(p=p,c=C,st=st.lower())
        stat_img=crop(image_path=topo_stat)
        axs[0,2].set_title('t-values')
        axs[ind,2].imshow(stat_img)
        axs[ind,2].axis('off')

        topo_DA=DA_file.format(p=p,clf=clf_name,c=C,st=st.lower())
        DA_img=crop(image_path=topo_DA)
        axs[0,3].set_title('DA')
        axs[ind,3].imshow(DA_img)
        axs[ind,3].axis('off')
    plt.savefig('test_subplot.tiff',dpi=650)
    plt.show()
