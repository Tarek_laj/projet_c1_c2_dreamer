import os
import numpy as np
from pylab import *
from itertools import product
import scipy.io as sio
import matplotlib.pyplot as plt
#%matplotlib notebook
from matplotlib import gridspec
#from mpl_toolkits.axes_grid1 import make_axes_locatable
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from mne import *
from ttest_perm_indep import ttest_perm_unpaired
from classification_utils import get_classifier,run_classification,load_pe
from sklearn.model_selection import StratifiedKFold

path='/home/karim/MATLAB/Work/Projet C1 C2/permutation entropy/entropy_per_stage/'



save_path='/home/karim/Projet_C1_C2_dreamer/Classification_Results/Permutation_entropy/'
if not os.path.exists(save_path):
    os.mkdir(save_path)
#path='/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/'
sensors_pos = sio.loadmat('/home/karim/MATLAB/Work/Projet C1 C2/Coord_2D_Slp_EEG.mat')['Cor']
ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']
moy=1
classification_mode='sf'
stat=True
clf_name='LDA'
test=False
n_perms=1000
outer_cv= StratifiedKFold(5)
print('Params info : \n classification mode = {cl} \n Test mode : {test}'.format(cl=classification_mode,
                                                                                 test=str(test)))
print('Classifier: {clf} \n stat: {sts} with n_perms={n} '.format(clf=clf_name,
                                                                     sts=str(stat),
                                                                     n=str(n_perms)))

if test==False:
    stade=['AWA','S1','S2','SWS','Rem']
else:
    stade=['S2']

for st in stade:
    print('Running classification for stage {st} '.format(st=st))
    sDreamer = np.arange(18) +1
    sNnDreamer = np.array([k for k in range(19, 37)])
    Dr_data=np.array([])
    nDr_data=np.array([])

    for s1 in range(sDreamer.shape[0]):
        M1=load_pe(path=path,subject=sDreamer[s1],stade=st,moy=moy)
        Dr_data=np.concatenate((Dr_data,M1),axis=0) if Dr_data.size else M1

    for s2 in range(sNnDreamer.shape[0]):
        M2=load_pe(path=path,subject=sNnDreamer[s2],stade=st,moy=moy)
        nDr_data=np.concatenate((nDr_data,M2),axis=0) if nDr_data.size else M2

    X=np.vstack((Dr_data,nDr_data))
    y=np.concatenate([np.ones(Dr_data.shape[0]),np.zeros(nDr_data.shape[0])],axis=0)

    test_scores,permutation_scores,pvalues=run_classification(clf_name=clf_name,
                                                                X=X,
                                                                y=y,
                                                                outer_cv=outer_cv,
                                                                mode=classification_mode,
                                                                stat=stat,
                                                                nb_permutations=n_perms,
                                                                n_jobs=-1)


    np.save(save_path+'DA_SF_{clf}_{sd}.npy'.format(clf=clf_name,sd=st),
    test_scores)
    np.save(save_path+'DA_perm_{clf}_{sd}.npy'.format(clf=clf_name,sd=st),
    permutation_scores)
    np.save(save_path+'pvals_{clf}_{sd}.npy'.format(clf=clf_name,sd=st),
    pvalues)
