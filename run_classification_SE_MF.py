# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 12:14:09 2017

@author: karim
"""
import os
import numpy as np
from pylab import *
from itertools import product
import scipy.io as sio
import matplotlib.pyplot as plt
#%matplotlib notebook
from matplotlib import gridspec
#from mpl_toolkits.axes_grid1 import make_axes_locatable
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from mne import *
from ttest_perm_indep import ttest_perm_unpaired
from classification_utils import get_classifier,run_classification,load_pe,load_C1C2
from sklearn.model_selection import StratifiedKFold


pe_path='/home/karim/MATLAB/Work/Projet C1 C2/permutation entropy/entropy_per_stage/'
path='/home/karim/projet_c1_c2_dreamer/Cumulants/'
# save_path='/home/karim/Projet_C1_C2_dreamer/Classification_Results/'
# if not os.path.exists(save_path):
#     os.mkdir(save_path)
#path='/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/'
sensors_pos = sio.loadmat('/home/karim/projet_c1_c2_dreamer/Coord_2D_Slp_EEG.mat')['Cor']
ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']
moy=1
classification_mode='mf_sg_elect'
add_pe=True
stat=True
clf_name='RBF_svm'
test=False
n_perms=1000
outer_cv= StratifiedKFold(5)
print('Params info : \n classification mode = {cl} \n Test mode : {test}'.format(cl=classification_mode,
                                                                                 test=str(test)))
print('Classifier: {clf} \n stat: {sts} with n_perms={n} '.format(clf=clf_name,
                                                                     sts=str(stat),
                                                                     n=str(n_perms)))

if test==False:
    stade=['AWA','S1','S2','SWS','Rem'];
    Cds=['c1','c2']
    cols=['col1','col2','col3','col4']
else:
    stade=['s2'];
    Cds=['c1','c2']
    cols=['col1']
for col, st in product (cols,stade):
    x_data=np.array([])
    X=[]
    for C in Cds:
        print('loading data  {c} {cl} {st} '.format(c=C,cl=col,st=st))
        sDreamer = np.arange(18) +1
        sNnDreamer = np.array([k for k in range(19, 37)])
        Dr_data=np.array([])
        nDr_data=np.array([])
        for s1,s2 in zip (range(sDreamer.shape[0]),range(sNnDreamer.shape[0])):

            M1=load_C1C2(sDreamer[s1],C,st.lower(),moy,path,col)
            Dr_data=np.concatenate((Dr_data,M1),axis=0) if Dr_data.size else M1
            M2=load_C1C2(sNnDreamer[s2],C,st.lower(),moy,path,col)
            nDr_data=np.concatenate((nDr_data,M2),axis=0) if nDr_data.size else M2
        x=np.vstack((Dr_data,nDr_data))
        print(x.shape)
        x_data=np.dstack((x_data,x)) if x_data.size else x
    if add_pe:
        pndr,pdr=np.array([]),np.array([])
        print('loading Permutation entropy: ')
        for s1,s2 in zip(range(sDreamer.shape[0]),range(sNnDreamer.shape[0])):
            p1=load_pe(path=pe_path,subject=sDreamer[s1],stade=st,moy=moy)
            pdr=np.concatenate((pdr,p1),axis=0) if pdr.size else p1
            p2=load_pe(path=pe_path,subject=sNnDreamer[s2],stade=st,moy=moy)
            pndr=np.concatenate((pndr,p2),axis=0) if pndr.size else p2
        xpe=np.vstack((pdr,pndr))
        x_data=np.dstack((x_data,xpe))
    print('Running Classification: ')
    n_elect=x_data.shape[1]
    for n in range(n_elect):
        X.append(np.squeeze(x_data[:,n,:]))
    print(X[0].shape)
    
    y=np.concatenate([np.ones(sDreamer.shape[0]),np.zeros(sNnDreamer.shape[0])],axis=0)
    test_scores,permutation_scores,pvalues=run_classification(clf_name=clf_name,
                                                            X=X,
                                                            y=y,
                                                            outer_cv=outer_cv,
                                                            mode=classification_mode,
                                                            stat=stat,
                                                            nb_permutations=n_perms,
                                                            n_jobs=-1)
    print(len(test_scores))
    sv=save_path+'/{col}/multifeat_single_electrode/DA'.format(col=col)
    if not os.path.exists(sv):
        os.makedirs(sv)
    np.save(sv+'/DA_C1C2Pe_{clf}_{sd}_{cl}.npy'.format(clf=clf_name,sd=st,cl=col),
    test_scores)
    np.save(sv+'/DA_perm_C1C2Pe_{clf}_{sd}_{cl}.npy'.format(clf=clf_name,sd=st,cl=col),
    permutation_scores)
    np.save(sv+'/pvals_C1C2Pe_{clf}_{sd}_{cl}.npy'.format(clf=clf_name,sd=st,cl=col),
    pvalues)
