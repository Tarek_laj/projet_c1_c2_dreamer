from classification_utils import Topo_DA
import numpy as np
import os
import scipy.io as sio
import matplotlib.pyplot as plt
from matplotlib import gridspec
from mne.viz import plot_topomap
from ttest_perm_indep import ttest_perm_unpaired


def load_data(subject,C,stade,moy,path):
    file='/lin_C1C2C3_guy{s}_{st}_{cl}.mat'
    ck = sio.loadmat(path+file.format(s=subject,st=stade,cl=col))[C][0:19,:]
    mat=np.mean(ck,axis=1)
    M=np.reshape(mat,(1,19))
    return np.array(M)

sensors_pos = sio.loadmat('/home/karim/MATLAB/Work/Projet C1 C2/Coord_2D_Slp_EEG.mat')['Cor']
#path='/home/karim/Projet_C1_C2_dreamer/Classification_Results/Permutation_entropy/'
stages=['awa','s1','s2','sws','rem']
#stages=['AWA','S1','S2','SWS','Rem']
C=['c1','c2']
cols=['col4','col2']
clf_name='LDA'
moy=1
cumul_path='/home/karim/projet_c1_c2_dreamer/Cumulants/'
ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']

savepath = '/home/karim/results_C1C2/figures_finales/'


for col in cols:

    for c in C:
        fig=plt.figure()
        gs = gridspec.GridSpec(5, 4,figure=fig)
        gs.update(wspace=0, hspace=0)
        if not os.path.exists(savepath):
            os.makedirs(savepath)
        if c=='c1':
            if col=='col1':
                minax=0
                maxax=3.5
            elif col=='col2':
                minax=0
                maxax=1.5
            elif col=='col3':
                minax=0
                maxax=1
            elif col=='col4':
                minax=0
                maxax=1
        elif c=='c2':
            minax=-0.1
            maxax=0

        for idx,st in enumerate( stages):
            #colonne 1 Topo c1 c2 dreamer
            sDreamer = np.arange(18) +1
            Dr_data=np.array([])
            Dr='HDRF'
            for s1 in range(sDreamer.shape[0]):
                M1=load_data(sDreamer[s1],c,st,moy,cumul_path)
                Dr_data=np.concatenate((Dr_data,M1),axis=0) if Dr_data.size else M1
            idcol=0
            ax = fig.add_subplot(gs[idx, idcol])
            if idx==0:
                ax.set_title('HDRF',fontsize =12)
            if idcol==0:
                ax.set_ylabel(st.upper(),fontsize =12)

            # if idx==0:
            #     ax.set_title(p_names[idp],fontsize =12)

            ax1,_=plot_topomap(np.mean(Dr_data,
                                axis=0),
                                sensors_pos,
                                cmap='seismic',
                                show=False,
                                vmin=minax,
                                vmax=maxax,
                                mask = None,
                                extrapolate='local',
                                contours=False,
                                names=ch_names_info,
                                show_names=False)
            if idx==4:
                p0 = ax.get_position().get_points().flatten()
                ax_cbar1 = fig.add_axes([p0[0]+0.05,p0[1]-0.05, 0.09, 0.01])
                fig.colorbar(ax1, cax=ax_cbar1, orientation='horizontal',ticks=[minax,maxax/2, maxax])
            #colonne 2 Topo c1 c2 nndreamer
            sNnDreamer = np.array([k for k in range(19, 37)])
            nnDr_data=np.array([])
            nnDr='LDRF'
            for s2 in range(sNnDreamer.shape[0]):
                M2=load_data(sNnDreamer[s2],c,st,moy,cumul_path)
                nnDr_data=np.concatenate((nnDr_data,M2),axis=0) if nnDr_data.size else M2
            idcol=1
            ax = fig.add_subplot(gs[idx, idcol])
            if idx==0:
                ax.set_title('LDRF',fontsize =12)
            if idcol==0:
                ax.set_ylabel(st.upper(),fontsize =12)


            ax1,_=plot_topomap(np.mean(nnDr_data,
                                axis=0),
                                sensors_pos,
                                cmap='seismic',
                                show=False,
                                vmin=minax,
                                vmax=maxax,
                                mask = None,
                                extrapolate='local',
                                contours=False,
                                names=ch_names_info,
                                show_names=False)

            if idx==4:
                p0 = ax.get_position().get_points().flatten()
                ax_cbar1 = fig.add_axes([p0[0]+0.05,p0[1]-0.05, 0.09, 0.01])
                fig.colorbar(ax1, cax=ax_cbar1, orientation='horizontal',ticks=[minax,maxax/2, maxax])
            #colonne 3 Topo stat
            ax = fig.add_subplot(gs[idx, 2])
            if idx==0:
                ax.set_title('t-values',fontsize =12)
            # if idcol==True:
            #     ax.set_ylabel(st.upper(),fontsize =12)

            data, pvals = ttest_perm_unpaired(Dr_data, nnDr_data, n_perm=1000, correction="maxstat")
            mask_default = np.full((len(data)), False, dtype=bool)
            mask = np.array(mask_default)
            mask[pvals <= 0.05] = True
            mask_params = dict(marker='*', markerfacecolor='w', markersize=8) # significant sensors appearence
            ax1,_ = plot_topomap(data,sensors_pos,
                                cmap='viridis',
                                show=False,
                                contours=False,
                                vmin=-2,
                                vmax=2,
                                extrapolate='local',
                                mask = mask,
                                mask_params = mask_params,
                                names=ch_names_info,show_names=False)

            if idx==4:
                p0 = ax.get_position().get_points().flatten()
                ax_cbar1 = fig.add_axes([p0[0]+0.05,p0[1]-0.05, 0.09, 0.01])
                fig.colorbar(ax1, cax=ax_cbar1, orientation='horizontal',ticks=[-2,0, 2])

            #colonne 4 topo DA
            ax = fig.add_subplot(gs[idx, 3])
            if idx==0:
                ax.set_title('DA(%)',fontsize =12)
            # if idcol==True:
            #     ax.set_ylabel(st.upper(),fontsize =12)

            DA_path='/home/karim/results_C1C2/Classification_Results/{cl}/single_feature_single_electrode/DA/'.format(cl=col)
            DA_file=DA_path+'DA_SF_{clf}_{c}_{st}_{cl}.npy'.format(c=c,clf=clf_name,st=st,cl=col)
            DA_perm_file=DA_path+ 'DA_perm_{clf}_{c}_{st}_{cl}.npy'.format(c=c,clf=clf_name,st=st,cl=col)
            DA=np.load(DA_file)*100
            print(max(DA))
            DA_perm=np.sort(np.load(DA_perm_file),axis=1)
            DA_thr=np.max(DA_perm[:,950])*100
            print('Chance level : ', DA_thr)
            ax = fig.add_subplot(gs[idx, 3])
            mask_default = np.full((len(DA)), False, dtype=bool)
            mask = np.array(mask_default)
            mask[DA >= DA_thr] = True
            mask_params = dict(marker='*', markerfacecolor='w', markersize=8) # significant sensors appearence
            ax1,_ = plot_topomap(DA,sensors_pos,
                                cmap='inferno',
                                extrapolate='local',
                                show=False,
                                vmin=50,vmax=70,
                                contours=True,
                                mask = mask,
                                mask_params = mask_params)
            if idx==4:
                p0 = ax.get_position().get_points().flatten()
                ax_cbar1 = fig.add_axes([p0[0]+0.05,p0[1]-0.05, 0.09, 0.01])
                fig.colorbar(ax1, cax=ax_cbar1, orientation='horizontal',ticks=[50,60,70])


        plt.savefig(savepath + 'fig1_topo_results_{c}_{p}.png'.format(c=c,p=col), dpi = 300)
