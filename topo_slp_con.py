# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 12:14:09 2017

@author: karim
"""
import os
import numpy as np
from pylab import *
from itertools import product
import scipy.io as sio
import matplotlib.pyplot as plt
from matplotlib import gridspec
from mne.viz import plot_topomap
from mne import pick_types, find_layout
from mne import *
from ttest_perm_indep import ttest_perm_unpaired

data_to_plot='Sleep' #'Sleep'
col='col1' # 'col1' 'col2' 'col3' 'col4'

cuml=['c1','c2','c3']


def load_slp_data(subject,C,stade,moy,path,cl):
    file = 'lin_C1C2C3_guy{s}_{st}_{c}.mat'
    ck = sio.loadmat(path+file.format(s=subject,st=stade,c=cl))[C]
    mat=np.mean(ck,axis=1)#[0:19]
    M=np.reshape(mat,(1,63))
    return np.array(M)


#path='/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/'


if data_to_plot=='Sleep':
    stade=['awa','s1','s2','sws','rem'];
    nbr_suj=36
    sdata = np.arange(nbr_suj) +1
    path='/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/sleep/cumulants/'
    savepath = '/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/sleep/topoplots/{}/'.format(col)
    if not os.path.exists(savepath):
        os.makedirs(savepath)
    sensors_pos = sio.loadmat('/home/karim/MATLAB/Work/Projet C1 C2/Coord_2D_Slp_EEG.mat')['Cor']
    ch_names_info =['Fz','Cz','Pz','C3','C4','T3', 'T4', 'Fp1',
                'Fp2', 'O1', 'O2', 'F3','F4', 'P3', 'P4', 'FC1', 'FC2', 'CP1', 'CP2']


elif data_to_plot=='Consc':
    stade=['consc','unconsc']
    sdata = np.asarray([2,3,4,5,6,8,10])
    path='/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/consc/cumulants/'
    savepath = '/home/karim/Projet_C1_C2_dreamer/c1c2c3_values_newtoolbox/consc/topoplots/{}/'.format(col)
    if not os.path.exists(savepath):
        os.makedirs(savepath)
    sensors_pos = sio.loadmat('/home/karim/Projet_C1_C2_dreamer/coord_thomas.mat')['electrodes_sevo_new']
    ch_names_info=['E1','E2','E3','E4','E5','E6','E7','E8','E9','E10','E11',
    'E12','E13','E14','E15','E16','E17','E18','E19','E20','E21','E22','E23','E24',
	'E25','E26','E27','E28','E29','E30','E31','E32','E33',
    'E34','E35','E36','E37','E38','E39','E41','E42','E43','E44',
    'E46','E47','E48','E49','E50','E51','E52','E53','E54','E55',
    'E56','E57','E58','E59','E60','E61','E62','E63','E64','Cz']


for C in cuml:
    if C=='c2':
        minax=-0.1
        maxax=0
    elif C=='c1':
        minax=0
        maxax=1
    elif C=='c3':
        minax=-0.5
        maxax=0.5

    for st in stade:

        data=np.array([])

        for s1 in range(sdata.shape[0]):

            M1=load_slp_data(sdata[s1],C,st,1,path,col)
            data=np.concatenate((data,M1),axis=0) if data.size else M1


        print(C,st,col)

        if stat:
            tval4, pval4 = ttest_perm_unpaired(cond1, cond2, n_perm=100, correction="maxstat")
        fig = plt.figure()
        ax,_ = plot_topomap(np.mean(data,axis=0), sensors_pos,
                            cmap='seismic', show=False,
                            vmin=minax, vmax=maxax, mask = None,
                            contours=False,
                            names=ch_names_info,show_names=True)

        fig.colorbar(ax)
        plt.savefig(savepath + 'topo_lin_{C}_{st}_{c}.tiff'.format(c=col,C =C,st=st), dpi = 300)
