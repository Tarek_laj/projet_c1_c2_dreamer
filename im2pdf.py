import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import matplotlib.gridspec as gridspec
import matplotlib.image as mpimg
clf_name='LDA'
C='c2'
Classes=['Dr','nDr']
stades=['AWA','S1','S2','SWS','Rem']
test=False
if test:
    Cols=['col2']
else:
    Cols=['col1','col2','col3','col4']
classif='single_feature_single_electrode'

topo_mean_file='/home/karim/results_C1C2/topoplots/{p}/topo_{c}_{st}_{clas}_{p}.tiff'
tval_file='/home/karim/results_C1C2/topoplots/{p}/topo_stat_{p}_{c}_{st}.tiff'
DA_file='/home/karim/results_C1C2/Classification_Results/{p}/\
{classif}/Topoplots/DA_SF_{clf}_{c}_{st}_{p}.png'


def crop(image_path=None, saved_location=None):

    im = Image.open(image_path)
    w, h = im.size
    print(w,h)
    #im.show()
    coords=(900,50,w-500,h-50)
    cropped_image = im.crop(coords)
    if isinstance(saved_location,str):
        cropped_image.save(saved_location)
    return cropped_image

for p in Cols:
    fig=plt.figure(constrained_layout=True)
    gs = gridspec.GridSpec(5, 4,figure=fig)
    gs.update(wspace=0, hspace=0)
    for ind,st in enumerate(stades):
        #fig, axs = plt.subplots(1, 4, sharey=True)
        # add a big axes, hide frame
        # fig.add_subplot(111, frameon=False)
        # #hide tick and tick label of the big axes
        # plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
        # plt.grid(False)
        # plt.xlabel("common X")

        topo_dr=topo_mean_file.format(p=p,c=C,st=st.lower(),clas='Dr')
        #dr_img =  crop(image_path=topo_dr)
        dr_img=mpimg.imread(topo_dr)

        ax1 = fig.add_subplot(gs[ind, 0])
        ax1.imshow(dr_img)
        #ax1.set_title('HDRF')
        ax1.axis('off')
        ax1.set_ylabel(st,rotation='horizontal')



        topo_ndr=topo_mean_file.format(p=p,c=C,st=st.lower(),clas='Ndr')
        #ndr_img =  crop(image_path=topo_ndr)

        ndr_img=mpimg.imread(topo_ndr)

        ax2 = fig.add_subplot(gs[ind, 1])
        ax2.imshow(ndr_img)
        #ax2.set_title('LDRF')
        ax2.axis('off')

        topo_stat=tval_file.format(p=p,c=C,st=st.lower())
        #stat_img=crop(image_path=topo_stat)
        ax3 = fig.add_subplot(gs[ind, 2])
        stat_img=mpimg.imread(topo_stat)

        #ax3.set_title('t-values')
        ax3.imshow(stat_img)
        ax3.axis('off')

        topo_DA=DA_file.format(p=p,clf=clf_name,c=C,st=st.lower(),classif=classif)
        #DA_img=crop(image_path=topo_DA)
        ax4 = fig.add_subplot(gs[ind, 3])
        DA_img=mpimg.imread(topo_DA)

        #ax4.set_title('DA')
        ax4.imshow(DA_img)
        ax4.axis('off')
        #axs.set_ylabel()
        #fig.text(0.04, 0.5, 'common Y', va='center', rotation='horizontal')
    savefile='/home/karim/results_C1C2/figures/figure_{classif}_{c}_p{cl}.png'.format(classif=classif,cl=p,c=C)
    #plt.show()
    plt.savefig(savefile,dpi=650)
